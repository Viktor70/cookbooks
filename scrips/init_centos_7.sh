# Обновляемся, ставим пакеты, подключаем репозиторий
#yum install chrony
yum update -y
yum install  -y lsof bind-utils iftop htop wget jq traceroute 

# Настраиваем время
systemctl start chronyd
systemctl enable chronyd

# Настраиваем commands.log
cat > /etc/profile.d/logging.sh << EOF
export PROMPT_COMMAND='RETRN_VAL=\$?;logger -p local6.debug "\$(whoami) [\$$]: \$(history 1 | sed "s/^[ ]*[0-9]\+[ ]*//" ) [\$RETRN_VAL]"'
EOF

cat > /etc/rsyslog.d/commands.conf << EOF
local6.debug    /var/log/commands.log
EOF

# Убираем мусорные сообщения из messages
cat > /etc/rsyslog.d/ignore-systemd-session-slice.conf << EOF
if \$programname == "systemd" and (\$msg contains "Starting Session" or \$msg contains "Started Session" or \$msg contains "Created slice" or \$msg contains "Starting user-" or \$msg contains "Starting User Slice of" or \$msg contains "Removed session" or \$msg contains "Removed slice User Slice of" or \$msg contains "Stopping User Slice of") then stop
EOF

systemctl restart rsyslog

# Отключаем selinux
cat > /etc/selinux/config << EOF
# This file controls the state of SELinux on the system.
# SELINUX= can take one of these three values:
#     enforcing - SELinux security policy is enforced.
#     permissive - SELinux prints warnings instead of enforcing.
#     disabled - No SELinux policy is loaded.
SELINUX=disabled
# SELINUXTYPE= can take one of three values:
#     targeted - Targeted processes are protected,
#     minimum - Modification of targeted policy. Only selected processes are protected.
#     mls - Multi Level Security protection.
SELINUXTYPE=targeted
EOF

shutdown -r now
