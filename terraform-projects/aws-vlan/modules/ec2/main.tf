// Configure the EC2 instance in a public subnet

resource "aws_instance" "bastion" {
  ami                         = "ami-0010131fd5d2fda75"
  associate_public_ip_address = true
  instance_type               = "t2.micro"
  subnet_id                   = var.vpc.public_subnets[0]
  vpc_security_group_ids      = [var.sg_pub_id]
  user_data                   = templatefile("./init-data/bastion_data.yaml", {ip_addr = "10.0.1.2"})
  
  tags = {
    "Name" = "${var.namespace}-EC2-BASTION"
  }


}
