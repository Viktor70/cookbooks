// Configure the EC2 instance in a public subnet
resource "aws_instance" "ec2_public" {
  ami                         = "ami-0010131fd5d2fda75"
  associate_public_ip_address = true
  instance_type               = "t2.micro"
  key_name                    = var.key_name
  subnet_id                   = var.vpc.public_subnets[0]
  vpc_security_group_ids      = [var.sg_pub_id]

  tags = {
    "Name" = "${var.namespace}-EC2-BASTION"
  }


}

// Configure the EC2 instance in a private subnet
resource "aws_instance" "ec2_private" {
  ami                         = "ami-0010131fd5d2fda75"
  associate_public_ip_address = false
  instance_type               = "t2.micro"
  key_name                    = var.key_name
  subnet_id                   = var.vpc.private_subnets[0]
  vpc_security_group_ids      = [var.sg_priv_id]

  tags = {
    "Name" = "${var.namespace}-EC2-PUPPET"
  }

}