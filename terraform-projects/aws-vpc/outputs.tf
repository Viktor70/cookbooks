output "public_connection_string" {
  description = "Copy/Paste/Enter - You are in the matrix"
  value       = "ssh -i ${module.ssh-key.key_name}.pem centos@${module.ec2.public_ip}"
}

output "private_connection_string" {
  description = "Copy/Paste/Enter - You are in the private ec2 instance"
  value       = "ssh -A -i ${module.ssh-key.key_name}.pem -t centos@${module.ec2.public_ip} ssh centos@${module.ec2.private_ip}"
}