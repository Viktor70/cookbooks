output "public_connection_string" {
  description = "Copy/Paste/Enter - You are in the matrix"
  value       = "ssh -i ~/.ssh/id_rsa_git devops@${module.ec2.public_ip}"
}
